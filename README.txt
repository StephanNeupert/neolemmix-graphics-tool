NeoLemmix Graphics Tool

by Stephan Neupert

This is an application to manage graphic styles for NeoLemmix. It ensures that all sprites
are located in the correct folders, the additional information for interactive objects is
stored correctly and custom color themes are applied.

Compile informations:
- Build "NeoLemmix GraphicsTool.sln"
- The Graphics Tool requires C# 6.0 (or newer)
- There are no dependencies on external libraries.

To build on Linux, install Mono and run xbuild without any arguments. 
Note that the Graphics Tool was mainly written for Windows (as the NeoLemmix player is 
Windows-only), so graphical issues may be expected on Linux.

Any bugs, feature requests and support issues are currently handled via the lemmings forums at
  www.lemmingsforums.net
My nickname there is Nepster.  

Copyright: CC BY-NC 4.0 (2017)
