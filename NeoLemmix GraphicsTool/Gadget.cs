﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace NeoLemmix_GraphicsTool
{
    class Gadget : IComparable<Gadget>, IPiece
    {
        public Gadget(string imagePath)
        {
            try
            {
                Name = Path.GetFileNameWithoutExtension(imagePath);
                Image = Utility.CreateBitmapFromFile(imagePath);
            }
            catch
            {
                Name = "unknown";
                Image = new Bitmap(1, 1);
            }

            NumFrames = 1;
            GadgetType = C.GadgetType.None;
            TriggerRect = new Rectangle(0, 0, 1, 1);
            IsResizableHoriz = false;
            IsResizableVert = false;
            KeyFrame = 0;
            Sound = string.Empty;
            ColorSettings = new List<string>();

            string infoPath = Path.ChangeExtension(imagePath, ".nxmo");
            if (File.Exists(infoPath))
            {
                try
                {
                    LoadInfo(infoPath);
                }
                catch
                {
                    // do nothing more
                }
            }

            SplitImageInFrames();
        }

        public string Name { get; set; }
        public Bitmap Image { get; set; }
        public List<Bitmap> BitmapFrames;
        public int NumFrames;
        public C.GadgetType GadgetType;
        public Rectangle TriggerRect;
        public bool IsResizableHoriz;
        public bool IsResizableVert;
        public int KeyFrame;
        public string Sound;
        public List<string> ColorSettings;

        public void SplitImageInFrames()
        {
            int frameHeight = Image.Height / NumFrames;

            BitmapFrames = new List<Bitmap>();
            for (int i = 0; i < NumFrames; i++)
            {
                Rectangle cropRect = new Rectangle(0, i * frameHeight, Image.Width, frameHeight);
                Bitmap newFrame = Image.Clone(cropRect, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                BitmapFrames.Add(newFrame.Zoom(2));
            }
        }

        public int CompareTo(Gadget gadget2)
        {
            return this.Name.CompareTo(gadget2.Name);
        }

        private void LoadInfo(string infoPath)
        {
            FileParser parser = null;
            try
            {
                parser = new FileParser(infoPath);
                List<FileLine> fileLines;
                while ((fileLines = parser.GetNextLines()) != null)
                {
                    FileLine line = fileLines[0];
                    switch (line.Key)
                    {
                        case "FRAMES": NumFrames = line.Value; break;

                        case "TRIGGER_X": TriggerRect.X = line.Value; break;
                        case "TRIGGER_Y": TriggerRect.Y = line.Value; break;
                        case "TRIGGER_WIDTH": TriggerRect.Width = line.Value; break;
                        case "TRIGGER_HEIGHT": TriggerRect.Height = line.Value; break;

                        case "RESIZE_VERTICAL": IsResizableVert = true; break;
                        case "RESIZE_HORIZONTAL": IsResizableHoriz = true; break;
                        case "RESIZE_BOTH": IsResizableVert = true; IsResizableHoriz = true; break;

                        case "WINDOW": GadgetType = C.GadgetType.Hatch; break;
                        case "EXIT": GadgetType = C.GadgetType.Exit; break;
                        case "TRAP": GadgetType = C.GadgetType.Trap; break;
                        case "SINGLE_USE_TRAP": GadgetType = C.GadgetType.TrapOnce; break;
                        case "WATER": GadgetType = C.GadgetType.Water; break;
                        case "FIRE": GadgetType = C.GadgetType.Fire; break;
                        case "ONE_WAY_RIGHT": GadgetType = C.GadgetType.OWW_Right; break;
                        case "ONE_WAY_LEFT": GadgetType = C.GadgetType.OWW_Left; break;
                        case "ONE_WAY_DOWN": GadgetType = C.GadgetType.OWW_Down; break;
                        case "BUTTON": GadgetType = C.GadgetType.Button; break;
                        case "LOCKED_EXIT": GadgetType = C.GadgetType.ExitLocked; break;
                        case "PICKUP_SKILL": GadgetType = C.GadgetType.PickUp; break;
                        case "TELEPORTER": GadgetType = C.GadgetType.Teleporter; break;
                        case "RECEIVER": GadgetType = C.GadgetType.Receiver; break;
                        case "SPLITTER": GadgetType = C.GadgetType.Splitter; break;
                        case "UPDRAFT": GadgetType = C.GadgetType.Updraft; break;
                        case "SPLATPAD": GadgetType = C.GadgetType.Splat; break;
                        case "FORCE_RIGHT": GadgetType = C.GadgetType.Force_Right;  break;
                        case "FORCE_LEFT": GadgetType = C.GadgetType.Force_Left; break;
                        case "MOVING_BACKGROUND": GadgetType = C.GadgetType.Background; break;

                        case "KEY_FRAME": KeyFrame = line.Value; break;
                        case "SOUND": Sound = line.Text; break;

                        case "MASK":
                            {
                                ColorSettings.Add(" ");
                                ColorSettings.Add("$MASK");
                                foreach (FileLine newline in fileLines)
                                {
                                    if (newline != line)
                                    {
                                        ColorSettings.Add("  " + newline.Key.ToUpper() + " " + newline.Text);
                                    }
                                }
                                ColorSettings.Add("$END");

                                break;
                            }
                    }
                }

            }
            finally
            {
                parser?.DisposeStreamReader();
            }
        }

        /// <summary>
        /// Returns the number of pixels, the trigger reaches beyond the image
        /// </summary>
        /// <returns></returns>
        public int TriggerOversize()
        {
            int border = 0;
            border = Math.Max(border, -TriggerRect.Top * 2);
            border = Math.Max(border, TriggerRect.Bottom * 2 - BitmapFrames[0].Height);
            border = Math.Max(border, -TriggerRect.Left * 2);
            border = Math.Max(border, TriggerRect.Right * 2 - BitmapFrames[0].Width);
            return border;
        }

        /// <summary>
        /// Returns the size of the frame sprite with border.
        /// </summary>
        /// <returns></returns>
        public Size SpriteOversize()
        {
            return new Size(BitmapFrames[0].Width + 2 * TriggerOversize(), BitmapFrames[0].Height + 2 * TriggerOversize());
        }
    }
}
