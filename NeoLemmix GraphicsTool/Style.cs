﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;

namespace NeoLemmix_GraphicsTool
{
    class Style
    {
        public Style()
        {
            Name = string.Empty;
            ThemeColors = C.DefaultThemeColor;
            Terrains = new List<Terrain>();
            Gadgets = new List<Gadget>();
            Backgrounds = new List<Background>();
        }

        public string Name;
        public Dictionary<C.ThemeColor, Color> ThemeColors;
        public List<Terrain> Terrains;
        public List<Gadget> Gadgets;
        public List<Background> Backgrounds;

        // Gets the pure 6-character hex calue of a color
        public string GetHexColor(C.ThemeColor themeColor)
        {
            Color color = ThemeColors[themeColor];
            return color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }

        // Gets a list of all pieces
        public List<IPiece> Pieces => new List<IPiece>().Concat(Terrains).Concat(Gadgets).Concat(Backgrounds).ToList();
    }
}
