﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Linq;

namespace NeoLemmix_GraphicsTool
{
  static class Save
  {
    static public bool Style(Style curStyle)
    {
      style = curStyle;
      style.Name = Utility.MakeFileNameCompliant(style.Name);

      // Get folder path to save to.
      folderPath = Utility.GetDirectoryPath();
      if (folderPath == null) return false;

      // Add the style name as a subfolder to the folderPath
      folderPath = Path.Combine(folderPath, string.IsNullOrWhiteSpace(style.Name) ? "unknown" : style.Name);

      if (Directory.Exists(folderPath))
      {
        // Display warning if folder already exists
        if (!AskOverwriteFiles()) return false;
        Directory.Delete(folderPath, true);
      }

      // (Re)create the target folder. 
      Directory.CreateDirectory(folderPath);

      // Make piece names windows-compliant
      CheckPieceNames();
      // Write style infos
      WriteThemeFile();
      WriteTerrainFiles();
      WriteGadgetFiles();
      WriteBackgroundFiles();

      return true;
    }

    static string folderPath = string.Empty;
    static Style style;

    ///// <summary>
    ///// Opens the file browser to select a folder to save the pack in.
    ///// </summary>
    ///// <returns></returns>
    //static private void GetSaveFolder()
    //{
    //  using (SaveFileDialog saveFileBrowser = new SaveFileDialog())
    //  {
    //    saveFileBrowser.InitialDirectory = C.StylePath;
    //    saveFileBrowser.FileName = "Please select the styles folder.";
    //    saveFileBrowser.Filter = "|";
    //    saveFileBrowser.RestoreDirectory = true;

    //    if (saveFileBrowser.ShowDialog() == DialogResult.OK)
    //    {
    //      folderPath = Path.GetDirectoryName(saveFileBrowser.FileName);
    //    }
    //  }
    //}

    /// <summary>
    /// Asks the user whether to continue with possible data loss.
    /// </summary>
    /// <returns></returns>
    static private bool AskOverwriteFiles()
    {
      string text = "The selected folder contains already files. Saving the style will overwrite resp. delete all of them. Continue?";
      return MessageBox.Show(text, "Overwrite Files?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
    }

    /// <summary>
    /// Checks that all piece names have names allowed as file names.
    /// </summary>
    static private void CheckPieceNames()
    {
      //First remove all invalid characters
      style.Pieces.ForEach(piece => piece.Name = Utility.MakeFileNameCompliant(piece.Name));

      // Check that no name appears more than once.
      foreach (IPiece piece in style.Pieces)
      {
        Type pieceType = piece.GetType();
        var collisions = style.Pieces.FindAll(p => p.Name.Equals(piece.Name) && p.GetType().Equals(pieceType));
        if (collisions.Count > 1)
        {
          // Add numbers to the piece names
          int index = 0;
          for (int i = 0; i < collisions.Count; i++)
          {
            // Check whether other pieces already have the proposed new name
            string newName;
            do
            {
              index++;
              newName = collisions[i].Name + "_" + index.ToString().PadLeft(2, '0');
            } while (style.Pieces.Exists(p => p.Name.Equals(newName) && p.GetType().Equals(pieceType)));
            collisions[i].Name = newName;
          }
        }
      }
    }

    /// <summary>
    /// Writes the global theme file.
    /// </summary>
    static private void WriteThemeFile()
    {
      File.Create(folderPath + C.DirSep + "theme.nxtm").Close();
      using (TextWriter textFile = new StreamWriter(folderPath + C.DirSep + "theme.nxtm", true))
      {
        textFile.WriteLine("LEMMINGS default");
        textFile.WriteLine(" ");
        textFile.WriteLine("$COLORS ");
        textFile.WriteLine("  MASK x" + style.GetHexColor(C.ThemeColor.Mask));
        textFile.WriteLine("  MINIMAP x" + style.GetHexColor(C.ThemeColor.Minimap));
        textFile.WriteLine("  BACKGROUND x" + style.GetHexColor(C.ThemeColor.Background));
        textFile.WriteLine("  ONE_WAYS x" + style.GetHexColor(C.ThemeColor.OneWayWall));
        textFile.WriteLine("  PICKUP_BORDER x" + style.GetHexColor(C.ThemeColor.PickUpBorder));
        textFile.WriteLine("  PICKUP_INSIDE x" + style.GetHexColor(C.ThemeColor.PickUp));
        textFile.WriteLine("$END ");
      }
    }

    /// <summary>
    /// Saves all terrain pieces.
    /// </summary>
    static private void WriteTerrainFiles()
    {
      if (style.Terrains.Count == 0) return;

      string terrainPath = folderPath + C.DirSep + "terrain";
      Directory.CreateDirectory(terrainPath);

      foreach (Terrain piece in style.Terrains)
      {
        piece.Image.Save(terrainPath + C.DirSep + piece.Name + ".png", ImageFormat.Png);
        // Save info file, if needed 
        if (piece.IsSteel)
        {
          string infoPath = terrainPath + C.DirSep + piece.Name + ".nxmt";
          File.Create(infoPath).Close();
          using (TextWriter textFile = new StreamWriter(infoPath, true))
          {
            textFile.WriteLine("STEEL");
          }
        }
      }
    }

    /// <summary>
    /// Saves all gadget pieces.
    /// </summary>
    static private void WriteGadgetFiles()
    {
      if (style.Gadgets.Count == 0) return;

      string gadgetPath = folderPath + C.DirSep + "objects";
      Directory.CreateDirectory(gadgetPath);

      foreach (Gadget piece in style.Gadgets)
      {
        piece.Image.Save(gadgetPath + C.DirSep + piece.Name + ".png", ImageFormat.Png);
        string infoPath = gadgetPath + C.DirSep + piece.Name + ".nxmo";
        WriteGadgetInfoFile(piece, infoPath);
      }
    }


    /// <summary>
    /// Saves all gadget pieces.
    /// </summary>
    static private void WriteGadgetInfoFile(Gadget piece, string infoPath)
    {
      File.Create(infoPath).Close();
      using (TextWriter textFile = new StreamWriter(infoPath, true))
      {
        textFile.WriteLine("FRAMES " + piece.NumFrames.ToString());
        // Add gadget type
        switch (piece.GadgetType)
        {
          case C.GadgetType.None: textFile.WriteLine("NO_EFFECT "); break;
          case C.GadgetType.Hatch: textFile.WriteLine("WINDOW "); break;
          case C.GadgetType.Exit: textFile.WriteLine("EXIT "); break;
          case C.GadgetType.Trap: textFile.WriteLine("TRAP "); break;
          case C.GadgetType.TrapOnce: textFile.WriteLine("SINGLE_USE_TRAP "); break;
          case C.GadgetType.Water: textFile.WriteLine("WATER "); break;
          case C.GadgetType.Fire: textFile.WriteLine("FIRE "); break;
          case C.GadgetType.OWW_Right: textFile.WriteLine("ONE_WAY_RIGHT "); break;
          case C.GadgetType.OWW_Left: textFile.WriteLine("ONE_WAY_LEFT "); break;
          case C.GadgetType.OWW_Down: textFile.WriteLine("ONE_WAY_DOWN "); break;
          case C.GadgetType.PickUp: textFile.WriteLine("PICKUP_SKILL "); break;
          case C.GadgetType.Teleporter: textFile.WriteLine("TELEPORTER "); break;
          case C.GadgetType.Receiver: textFile.WriteLine("RECEIVER "); break;
          case C.GadgetType.ExitLocked: textFile.WriteLine("LOCKED_EXIT "); break;
          case C.GadgetType.Button: textFile.WriteLine("BUTTON "); break;
          case C.GadgetType.Updraft: textFile.WriteLine("UPDRAFT "); break;
          case C.GadgetType.Splat: textFile.WriteLine("SPLATPAD "); break;
          case C.GadgetType.Force_Right: textFile.WriteLine("FORCE_RIGHT "); break;
          case C.GadgetType.Force_Left: textFile.WriteLine("FORCE_LEFT "); break;
          case C.GadgetType.Splitter: textFile.WriteLine("SPLITTER "); break;
          case C.GadgetType.Background: textFile.WriteLine("MOVING_BACKGROUND "); break;
        }
        // Add trigger area
        if (C.HasTrigger(piece))
        {
          textFile.WriteLine("TRIGGER_X " + piece.TriggerRect.X.ToString());
          textFile.WriteLine("TRIGGER_Y " + piece.TriggerRect.Y.ToString());
        }
        if (C.HasTriggerArea(piece))
        {
          textFile.WriteLine("TRIGGER_WIDTH " + piece.TriggerRect.Width.ToString());
          textFile.WriteLine("TRIGGER_HEIGHT " + piece.TriggerRect.Height.ToString());
        }

        // Add resizability
        if (C.IsResizable(piece))
        {
          if (piece.IsResizableHoriz)
          {
            textFile.WriteLine("RESIZE_HORIZONTAL ");
          }
          if (piece.IsResizableVert)
          {
            textFile.WriteLine("RESIZE_VERTICAL ");
          }
        }
        // Add keyframe
        if (C.HasKeyFrame(piece))
        {
          textFile.WriteLine("KEY_FRAME " + piece.KeyFrame.ToString());
        }
        // Add sound
        if (C.HasSound(piece) && !string.IsNullOrWhiteSpace(piece.Sound))
        {
          textFile.WriteLine("SOUND " + Path.GetFileNameWithoutExtension(piece.Sound));
        }
        // Add any color setting there might be
        foreach (string line in piece.ColorSettings)
        {
          textFile.WriteLine(line);
        }
      }

    }

    /// <summary>
    /// Saves all background images.
    /// </summary>
    static private void WriteBackgroundFiles()
    {
      if (style.Backgrounds.Count == 0) return;

      string backgroundPath = folderPath + C.DirSep + "backgrounds";
      Directory.CreateDirectory(backgroundPath);

      foreach (Background piece in style.Backgrounds)
      {
        piece.Image.Save(backgroundPath + C.DirSep + piece.Name + ".png", ImageFormat.Png);
      }
    }
  }
}
