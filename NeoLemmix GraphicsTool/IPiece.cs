﻿using System.Drawing;

namespace NeoLemmix_GraphicsTool
{
    interface IPiece
    {
        string Name { get; set; }
        Bitmap Image { get; set; }
    }
}
