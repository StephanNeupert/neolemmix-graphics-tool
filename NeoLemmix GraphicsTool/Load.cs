﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;

namespace NeoLemmix_GraphicsTool
{
  static class Read
  {
    public static Style Style()
    {
      folderPath = Utility.GetDirectoryPath();
      if (folderPath == null) return null;

      newStyle = new Style();

      LoadThemeInfo();
      LoadTerrainPieces();
      LoadGadgetPieces();
      LoadBackgroundPieces();

      return newStyle;
    }

    static string folderPath;
    static Style newStyle;

    /// <summary>
    /// Loads the global theme colors.
    /// </summary>
    static private void LoadThemeInfo()
    {
      newStyle.Name = Path.GetFileName(folderPath);

      string filePath = folderPath + C.DirSep + "theme.nxtm";
      if (!File.Exists(filePath)) return;

      FileParser parser = null;
      try
      {
        parser = new FileParser(filePath);
        List<FileLine> fileLines;
        while ((fileLines = parser.GetNextLines()) != null)
        {
          if (fileLines[0].Key == "COLORS")
          {
            foreach (FileLine line in fileLines)
            {
              switch (line.Key)
              {
                case "MASK": SetColor(C.ThemeColor.Mask, line.Text); break;
                case "MINIMAP": SetColor(C.ThemeColor.Minimap, line.Text); break;
                case "BACKGROUND": SetColor(C.ThemeColor.Background, line.Text); break;
                case "ONE_WAYS": SetColor(C.ThemeColor.OneWayWall, line.Text); break;
                case "PICKUP_BORDER": SetColor(C.ThemeColor.PickUpBorder, line.Text); break;
                case "PICKUP_INSIDE": SetColor(C.ThemeColor.PickUp, line.Text); break;
              }
            }
          }
        }

      }
      finally
      {
        parser?.DisposeStreamReader();
      }
    }

    /// <summary>
    /// Transforms a NeoLemmix color hex string to a Color.
    /// </summary>
    /// <param name="hexstring"></param>
    /// <returns></returns>
    static private void SetColor(C.ThemeColor themeColor, string hexstring)
    {
      newStyle.ThemeColors[themeColor] = hexstring == "xlack" ?
        C.DefaultThemeColor[themeColor] :
        ColorTranslator.FromHtml(hexstring.Replace('x', '#'));
    }


    /// <summary>
    /// Loads all terrain pieces.
    /// </summary>
    static private void LoadTerrainPieces()
    {
      string terrainPath = folderPath + C.DirSep + "terrain";
      if (!Directory.Exists(terrainPath)) return;

      string[] imagePaths = Directory.GetFiles(terrainPath, "*.png");
      foreach (string imagePath in imagePaths)
      {
        newStyle.Terrains.Add(new Terrain(imagePath));
      }
    }

    /// <summary>
    /// Loads all gadget pieces.
    /// </summary>
    static private void LoadGadgetPieces()
    {
      string terrainPath = folderPath + C.DirSep + "objects";
      if (!Directory.Exists(terrainPath)) return;

      string[] imagePaths = Directory.GetFiles(terrainPath, "*.png");
      foreach (string imagePath in imagePaths)
      {
        newStyle.Gadgets.Add(new Gadget(imagePath));
      }
    }

    /// <summary>
    /// Loads all background pieces.
    /// </summary>
    static private void LoadBackgroundPieces()
    {
      string terrainPath = folderPath + C.DirSep + "backgrounds";
      if (!Directory.Exists(terrainPath)) return;

      string[] imagePaths = Directory.GetFiles(terrainPath, "*.png");
      foreach (string imagePath in imagePaths)
      {
        newStyle.Backgrounds.Add(new Background(imagePath));
      }
    }



  }
}
