﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace NeoLemmix_GraphicsTool
{
  static class Utility
  {
    /// <summary>
    /// Checks if an object is contained in an array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public static bool In<T>(this T obj, params T[] args)
    {
      return args.Contains(obj);
    }

    /// <summary>
    /// Get the folder path for the style
    /// </summary>
    public static string GetDirectoryPath()
    {
      var folderBrowser = new FolderBrowser.FolderBrowser2();
      folderBrowser.DirectoryPath = C.StylePath;
      var result = folderBrowser.ShowDialog(null);
      return result == DialogResult.OK ? folderBrowser.DirectoryPath : null;
    }

    /// <summary>
    /// Opens the file browser and returns the selected files.
    /// </summary>
    /// <param name="Filter"></param>
    /// <param name="isMultiselect"></param>
    /// <returns></returns>
    public static string[] SelectFiles(string filter, bool isMultiselect, string startFolder = null)
    {
      var openFileDialog = new OpenFileDialog();

      openFileDialog.InitialDirectory = (startFolder != null) ? startFolder : C.AppPath;
      openFileDialog.Multiselect = isMultiselect;
      openFileDialog.Filter = filter;
      openFileDialog.RestoreDirectory = true;
      openFileDialog.CheckFileExists = true;

      string[] filePaths = new string[0];

      try
      {
        if (openFileDialog.ShowDialog() == DialogResult.OK)
        {
          filePaths = openFileDialog.FileNames;
        }
      }
      catch (Exception Ex)
      {
        MessageBox.Show("Error while showing the file browser." + C.NewLine + Ex.Message, "File browser error");
      }
      finally
      {
        openFileDialog?.Dispose();
      }

      return filePaths;
    }

    /// <summary>
    /// Loads a bitmap from a file and closes the file again.
    /// <para> Exception handling has to be done by the calling method. </para>
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static Bitmap CreateBitmapFromFile(string filePath)
    {
      Bitmap image;
      using (var tempImage = new Bitmap(filePath))
      {
        image = new Bitmap(tempImage);
      }
      return image;
    }

    /// <summary>
    /// Replaces forbidden characters and whitespaces in to-be file-names.
    /// </summary>
    /// <param name="oldPath"></param>
    /// <returns></returns>
    public static string MakeFileNameCompliant(string oldPath)
    {
      return oldPath.Replace(' ', '_')
                    .Replace('<', '_')
                    .Replace('>', '_')
                    .Replace(':', '_')
                    .Replace('"', '_')
                    .Replace('/', '_')
                    .Replace('\\', '_')
                    .Replace('|', '_')
                    .Replace('?', '_')
                    .Replace('*', '_');
    }

    /// <summary>
    /// Handles a global unexpected exception and displays a warning message to the user.
    /// </summary>
    /// <param name="Ex"></param>
    public static void HandleGlobalException(Exception Ex)
    {
      try
      {
        Utility.LogException(Ex);
        string errorString = "Klopt niet: " + Ex.Message + C.NewLine + "Try to continue working on the style? Selecting 'no' will quit the style editor.";
        var result = MessageBox.Show(errorString, "Error", MessageBoxButtons.YesNo);
        if (result == DialogResult.No) Application.Exit();
      }
      catch
      {
        Application.Exit();
      }
    }

    /// <summary>
    /// Logs an exception message to AppPath/ErrorLog.txt.
    /// </summary>
    /// <param name="ex"></param>
    public static void LogException(Exception ex)
    {
      string errorPath = C.AppPath + "ErrorLog.txt";
      using (System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true))
      {
        textFile.WriteLine(ex.ToString());
      }
    }

    /// <summary>
    /// Copies the bytes of the NewPixel to the pixel pointed to in the first argument.
    /// </summary>
    /// <param name="ptrToPixel"></param>
    /// <param name="ptrToNewPixel"></param>
    private static unsafe void ChangePixel(byte* ptrToPixel, byte* ptrToNewPixel)
    {
      ptrToPixel[0] = ptrToNewPixel[0];
      ptrToPixel[1] = ptrToNewPixel[1];
      ptrToPixel[2] = ptrToNewPixel[2];
      ptrToPixel[3] = 255;
    }

    /// <summary>
    /// Zooms a bitmap and crops it to a smaller size.
    /// </summary>
    /// <param name="origBmp"></param>
    /// <param name="zoomFactor"></param>
    /// <param name="newBmpSize"></param>
    /// <returns></returns>
    const int BytesPerPixel = 4;
    public static Bitmap Zoom(this Bitmap origBmp, int zoomFactor)
    {
      int origBmpWidth = origBmp.Width;
      int origBmpHeight = origBmp.Height;
      Bitmap newBmp = new Bitmap(origBmpWidth * zoomFactor, origBmpHeight * zoomFactor);

      unsafe
      {
        // Get pointer to first pixel of OldBitmap
        BitmapData origBmpData = origBmp.LockBits(new Rectangle(0, 0, origBmp.Width, origBmp.Height), ImageLockMode.ReadOnly, origBmp.PixelFormat);
        byte* ptrOrigFirstPixel = (byte*)origBmpData.Scan0;
        Debug.Assert(Bitmap.GetPixelFormatSize(origBmp.PixelFormat) == 32, "Zoomed Bitmap has no alpha channel!");

        // Get pointer to first pixel of NewBitmap
        BitmapData newBmpData = newBmp.LockBits(new Rectangle(0, 0, newBmp.Width, newBmp.Height), ImageLockMode.WriteOnly, newBmp.PixelFormat);
        byte* ptrNewFirstPixel = (byte*)newBmpData.Scan0;

        // Copy the pixels
        Parallel.For(0, origBmpHeight, y =>
        {
          byte* curOrigLine = ptrOrigFirstPixel + y * origBmpData.Stride;

          for (int i = 0; i < zoomFactor; i++)
          {
            byte* curNewLine = ptrNewFirstPixel + (y * zoomFactor + i) * newBmpData.Stride;

            for (int x = 0; x < origBmpWidth; x++)
            {
              for (int j = 0; j < zoomFactor; j++)
              {
                ChangePixel(curNewLine + (zoomFactor * x + j) * BytesPerPixel, curOrigLine + x * BytesPerPixel);
              }
            }
          }
        });

        origBmp.UnlockBits(origBmpData);
        newBmp.UnlockBits(newBmpData);
      }

      return newBmp;
    }
  }
}
