﻿using System;
using System.IO;
using System.Drawing;

namespace NeoLemmix_GraphicsTool
{
    class Background : IComparable<Background>, IPiece
    {
        public Background(string imagePath)
        {
            try
            {
                Name = Path.GetFileNameWithoutExtension(imagePath);
                Image = Utility.CreateBitmapFromFile(imagePath);
            }
            catch
            {
                Name = "unknown";
                Image = new Bitmap(1, 1);
            }
        }

        public string Name { get; set; }
        public Bitmap Image { get; set; }

        public int CompareTo(Background back2)
        {
            return this.Name.CompareTo(back2.Name);
        }
    }
}
