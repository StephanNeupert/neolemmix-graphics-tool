﻿namespace NeoLemmix_GraphicsTool
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tabMain = new System.Windows.Forms.TabControl();
      this.tabGeneral = new System.Windows.Forms.TabPage();
      this.picColor = new System.Windows.Forms.PictureBox();
      this.numBlue = new System.Windows.Forms.NumericUpDown();
      this.lblBlue = new System.Windows.Forms.Label();
      this.numGreen = new System.Windows.Forms.NumericUpDown();
      this.lblGreen = new System.Windows.Forms.Label();
      this.numRed = new System.Windows.Forms.NumericUpDown();
      this.lblRed = new System.Windows.Forms.Label();
      this.lblStyleColors = new System.Windows.Forms.Label();
      this.cmbColorTypes = new System.Windows.Forms.ComboBox();
      this.txtStyleName = new System.Windows.Forms.TextBox();
      this.lblStyleName = new System.Windows.Forms.Label();
      this.tabTerrain = new System.Windows.Forms.TabPage();
      this.picTerrain = new System.Windows.Forms.PictureBox();
      this.checkTerrainSteel = new System.Windows.Forms.CheckBox();
      this.txtTerrainName = new System.Windows.Forms.TextBox();
      this.lblTerrainName = new System.Windows.Forms.Label();
      this.butTerrainRemove = new System.Windows.Forms.Button();
      this.butTerrainAdd = new System.Windows.Forms.Button();
      this.listBoxTerrain = new System.Windows.Forms.ListBox();
      this.tabGadgets = new System.Windows.Forms.TabPage();
      this.txtGadgetSound = new System.Windows.Forms.TextBox();
      this.lblGadgetSound = new System.Windows.Forms.Label();
      this.numGadgetKeyFrame = new System.Windows.Forms.NumericUpDown();
      this.lblGadgetKeyFrame = new System.Windows.Forms.Label();
      this.picBoxGadget = new System.Windows.Forms.PictureBox();
      this.checkGadgetResizeVert = new System.Windows.Forms.CheckBox();
      this.checkGadgetResizeHoriz = new System.Windows.Forms.CheckBox();
      this.lblGadgetResize = new System.Windows.Forms.Label();
      this.numGadgetTriggerH = new System.Windows.Forms.NumericUpDown();
      this.lblGadgetTriggerH = new System.Windows.Forms.Label();
      this.numGadgetTriggerW = new System.Windows.Forms.NumericUpDown();
      this.lblGadgetTriggerW = new System.Windows.Forms.Label();
      this.numGadgetTriggerY = new System.Windows.Forms.NumericUpDown();
      this.lblGadgetTriggerY = new System.Windows.Forms.Label();
      this.numGadgetTriggerX = new System.Windows.Forms.NumericUpDown();
      this.lblGadgetTriggerX = new System.Windows.Forms.Label();
      this.lblGadgetTrigger = new System.Windows.Forms.Label();
      this.numGadgetFrames = new System.Windows.Forms.NumericUpDown();
      this.lblGadgetFrames = new System.Windows.Forms.Label();
      this.cmbGadgetType = new System.Windows.Forms.ComboBox();
      this.lblGadgetType = new System.Windows.Forms.Label();
      this.txtGadgetName = new System.Windows.Forms.TextBox();
      this.lblGadgetName = new System.Windows.Forms.Label();
      this.butGadgetRemove = new System.Windows.Forms.Button();
      this.butGadgetAdd = new System.Windows.Forms.Button();
      this.listBoxGadgets = new System.Windows.Forms.ListBox();
      this.tabBackground = new System.Windows.Forms.TabPage();
      this.txtBackgroundName = new System.Windows.Forms.TextBox();
      this.lblBackgroundName = new System.Windows.Forms.Label();
      this.picBackground = new System.Windows.Forms.PictureBox();
      this.butBackgroundRemove = new System.Windows.Forms.Button();
      this.butBackgroundAdd = new System.Windows.Forms.Button();
      this.listBoxBackgrounds = new System.Windows.Forms.ListBox();
      this.timerPicGadget = new System.Windows.Forms.Timer(this.components);
      this.menuStrip1.SuspendLayout();
      this.tabMain.SuspendLayout();
      this.tabGeneral.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picColor)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numBlue)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGreen)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numRed)).BeginInit();
      this.tabTerrain.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picTerrain)).BeginInit();
      this.tabGadgets.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetKeyFrame)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picBoxGadget)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerH)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerW)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetFrames)).BeginInit();
      this.tabBackground.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picBackground)).BeginInit();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.aboutToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(416, 24);
      this.menuStrip1.TabIndex = 0;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.exitToolStripMenuItem.Text = "Exit";
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
      // 
      // loadToolStripMenuItem
      // 
      this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
      this.loadToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
      this.loadToolStripMenuItem.Text = "Load";
      this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
      // 
      // saveToolStripMenuItem
      // 
      this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
      this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
      this.saveToolStripMenuItem.Text = "Save";
      this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
      // 
      // aboutToolStripMenuItem
      // 
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
      this.aboutToolStripMenuItem.Text = "About";
      this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
      // 
      // tabMain
      // 
      this.tabMain.Controls.Add(this.tabGeneral);
      this.tabMain.Controls.Add(this.tabTerrain);
      this.tabMain.Controls.Add(this.tabGadgets);
      this.tabMain.Controls.Add(this.tabBackground);
      this.tabMain.Location = new System.Drawing.Point(0, 27);
      this.tabMain.Name = "tabMain";
      this.tabMain.SelectedIndex = 0;
      this.tabMain.Size = new System.Drawing.Size(416, 414);
      this.tabMain.TabIndex = 1;
      // 
      // tabGeneral
      // 
      this.tabGeneral.Controls.Add(this.picColor);
      this.tabGeneral.Controls.Add(this.numBlue);
      this.tabGeneral.Controls.Add(this.lblBlue);
      this.tabGeneral.Controls.Add(this.numGreen);
      this.tabGeneral.Controls.Add(this.lblGreen);
      this.tabGeneral.Controls.Add(this.numRed);
      this.tabGeneral.Controls.Add(this.lblRed);
      this.tabGeneral.Controls.Add(this.lblStyleColors);
      this.tabGeneral.Controls.Add(this.cmbColorTypes);
      this.tabGeneral.Controls.Add(this.txtStyleName);
      this.tabGeneral.Controls.Add(this.lblStyleName);
      this.tabGeneral.Location = new System.Drawing.Point(4, 22);
      this.tabGeneral.Name = "tabGeneral";
      this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
      this.tabGeneral.Size = new System.Drawing.Size(408, 330);
      this.tabGeneral.TabIndex = 0;
      this.tabGeneral.Text = "General";
      this.tabGeneral.UseVisualStyleBackColor = true;
      // 
      // picColor
      // 
      this.picColor.BackColor = System.Drawing.Color.Black;
      this.picColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.picColor.Location = new System.Drawing.Point(139, 80);
      this.picColor.Name = "picColor";
      this.picColor.Size = new System.Drawing.Size(51, 49);
      this.picColor.TabIndex = 10;
      this.picColor.TabStop = false;
      // 
      // numBlue
      // 
      this.numBlue.Location = new System.Drawing.Point(58, 120);
      this.numBlue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.numBlue.Name = "numBlue";
      this.numBlue.Size = new System.Drawing.Size(46, 20);
      this.numBlue.TabIndex = 9;
      this.numBlue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numBlue.ValueChanged += new System.EventHandler(this.numColor_ValueChanged);
      // 
      // lblBlue
      // 
      this.lblBlue.AutoSize = true;
      this.lblBlue.Location = new System.Drawing.Point(25, 122);
      this.lblBlue.Name = "lblBlue";
      this.lblBlue.Size = new System.Drawing.Size(28, 13);
      this.lblBlue.TabIndex = 8;
      this.lblBlue.Text = "Blue";
      // 
      // numGreen
      // 
      this.numGreen.Location = new System.Drawing.Point(58, 94);
      this.numGreen.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.numGreen.Name = "numGreen";
      this.numGreen.Size = new System.Drawing.Size(46, 20);
      this.numGreen.TabIndex = 7;
      this.numGreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGreen.ValueChanged += new System.EventHandler(this.numColor_ValueChanged);
      // 
      // lblGreen
      // 
      this.lblGreen.AutoSize = true;
      this.lblGreen.Location = new System.Drawing.Point(25, 96);
      this.lblGreen.Name = "lblGreen";
      this.lblGreen.Size = new System.Drawing.Size(36, 13);
      this.lblGreen.TabIndex = 6;
      this.lblGreen.Text = "Green";
      // 
      // numRed
      // 
      this.numRed.Location = new System.Drawing.Point(58, 68);
      this.numRed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.numRed.Name = "numRed";
      this.numRed.Size = new System.Drawing.Size(46, 20);
      this.numRed.TabIndex = 5;
      this.numRed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numRed.ValueChanged += new System.EventHandler(this.numColor_ValueChanged);
      // 
      // lblRed
      // 
      this.lblRed.AutoSize = true;
      this.lblRed.Location = new System.Drawing.Point(25, 70);
      this.lblRed.Name = "lblRed";
      this.lblRed.Size = new System.Drawing.Size(27, 13);
      this.lblRed.TabIndex = 4;
      this.lblRed.Text = "Red";
      // 
      // lblStyleColors
      // 
      this.lblStyleColors.AutoSize = true;
      this.lblStyleColors.Location = new System.Drawing.Point(8, 43);
      this.lblStyleColors.Name = "lblStyleColors";
      this.lblStyleColors.Size = new System.Drawing.Size(72, 13);
      this.lblStyleColors.TabIndex = 3;
      this.lblStyleColors.Text = "Theme Colors";
      // 
      // cmbColorTypes
      // 
      this.cmbColorTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbColorTypes.FormattingEnabled = true;
      this.cmbColorTypes.Items.AddRange(new object[] {
            "Background",
            "Minimap",
            "One-way arrows",
            "Pick-up skill",
            "Pick-up skill border",
            "Mask"});
      this.cmbColorTypes.Location = new System.Drawing.Point(101, 40);
      this.cmbColorTypes.Name = "cmbColorTypes";
      this.cmbColorTypes.Size = new System.Drawing.Size(131, 21);
      this.cmbColorTypes.TabIndex = 2;
      this.cmbColorTypes.SelectedIndexChanged += new System.EventHandler(this.cmbColorTypes_SelectedIndexChanged);
      // 
      // txtStyleName
      // 
      this.txtStyleName.Location = new System.Drawing.Point(68, 8);
      this.txtStyleName.Name = "txtStyleName";
      this.txtStyleName.Size = new System.Drawing.Size(164, 20);
      this.txtStyleName.TabIndex = 1;
      this.txtStyleName.Text = "unknown";
      this.txtStyleName.TextChanged += new System.EventHandler(this.txtStyleName_TextChanged);
      // 
      // lblStyleName
      // 
      this.lblStyleName.AutoSize = true;
      this.lblStyleName.Location = new System.Drawing.Point(7, 11);
      this.lblStyleName.Name = "lblStyleName";
      this.lblStyleName.Size = new System.Drawing.Size(35, 13);
      this.lblStyleName.TabIndex = 0;
      this.lblStyleName.Text = "Name";
      // 
      // tabTerrain
      // 
      this.tabTerrain.Controls.Add(this.picTerrain);
      this.tabTerrain.Controls.Add(this.checkTerrainSteel);
      this.tabTerrain.Controls.Add(this.txtTerrainName);
      this.tabTerrain.Controls.Add(this.lblTerrainName);
      this.tabTerrain.Controls.Add(this.butTerrainRemove);
      this.tabTerrain.Controls.Add(this.butTerrainAdd);
      this.tabTerrain.Controls.Add(this.listBoxTerrain);
      this.tabTerrain.Location = new System.Drawing.Point(4, 22);
      this.tabTerrain.Name = "tabTerrain";
      this.tabTerrain.Padding = new System.Windows.Forms.Padding(3);
      this.tabTerrain.Size = new System.Drawing.Size(408, 388);
      this.tabTerrain.TabIndex = 1;
      this.tabTerrain.Text = "Terrain";
      this.tabTerrain.UseVisualStyleBackColor = true;
      this.tabTerrain.Enter += new System.EventHandler(this.tabTerrain_Enter);
      // 
      // picTerrain
      // 
      this.picTerrain.BackColor = System.Drawing.Color.Black;
      this.picTerrain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.picTerrain.Location = new System.Drawing.Point(144, 91);
      this.picTerrain.Name = "picTerrain";
      this.picTerrain.Size = new System.Drawing.Size(256, 293);
      this.picTerrain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.picTerrain.TabIndex = 25;
      this.picTerrain.TabStop = false;
      // 
      // checkTerrainSteel
      // 
      this.checkTerrainSteel.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.checkTerrainSteel.Location = new System.Drawing.Point(140, 67);
      this.checkTerrainSteel.Name = "checkTerrainSteel";
      this.checkTerrainSteel.Size = new System.Drawing.Size(73, 18);
      this.checkTerrainSteel.TabIndex = 10;
      this.checkTerrainSteel.Text = "Steel";
      this.checkTerrainSteel.UseVisualStyleBackColor = true;
      this.checkTerrainSteel.CheckStateChanged += new System.EventHandler(this.checkTerrainSteel_CheckStateChanged);
      // 
      // txtTerrainName
      // 
      this.txtTerrainName.Location = new System.Drawing.Point(202, 44);
      this.txtTerrainName.Name = "txtTerrainName";
      this.txtTerrainName.Size = new System.Drawing.Size(198, 20);
      this.txtTerrainName.TabIndex = 9;
      this.txtTerrainName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTerrainName_KeyDown);
      this.txtTerrainName.Leave += new System.EventHandler(this.txtTerrainName_Leave);
      // 
      // lblTerrainName
      // 
      this.lblTerrainName.AutoSize = true;
      this.lblTerrainName.Location = new System.Drawing.Point(141, 47);
      this.lblTerrainName.Name = "lblTerrainName";
      this.lblTerrainName.Size = new System.Drawing.Size(35, 13);
      this.lblTerrainName.TabIndex = 8;
      this.lblTerrainName.Text = "Name";
      // 
      // butTerrainRemove
      // 
      this.butTerrainRemove.Location = new System.Drawing.Point(219, 5);
      this.butTerrainRemove.Name = "butTerrainRemove";
      this.butTerrainRemove.Size = new System.Drawing.Size(71, 30);
      this.butTerrainRemove.TabIndex = 2;
      this.butTerrainRemove.Text = "Remove";
      this.butTerrainRemove.UseVisualStyleBackColor = true;
      this.butTerrainRemove.Click += new System.EventHandler(this.butTerrainRemove_Click);
      // 
      // butTerrainAdd
      // 
      this.butTerrainAdd.Location = new System.Drawing.Point(142, 5);
      this.butTerrainAdd.Name = "butTerrainAdd";
      this.butTerrainAdd.Size = new System.Drawing.Size(71, 30);
      this.butTerrainAdd.TabIndex = 1;
      this.butTerrainAdd.Text = "Add";
      this.butTerrainAdd.UseVisualStyleBackColor = true;
      this.butTerrainAdd.Click += new System.EventHandler(this.butTerrainAdd_Click);
      // 
      // listBoxTerrain
      // 
      this.listBoxTerrain.FormattingEnabled = true;
      this.listBoxTerrain.Location = new System.Drawing.Point(3, 3);
      this.listBoxTerrain.Name = "listBoxTerrain";
      this.listBoxTerrain.Size = new System.Drawing.Size(130, 381);
      this.listBoxTerrain.Sorted = true;
      this.listBoxTerrain.TabIndex = 0;
      this.listBoxTerrain.SelectedIndexChanged += new System.EventHandler(this.listBoxTerrain_SelectedIndexChanged);
      // 
      // tabGadgets
      // 
      this.tabGadgets.Controls.Add(this.txtGadgetSound);
      this.tabGadgets.Controls.Add(this.lblGadgetSound);
      this.tabGadgets.Controls.Add(this.numGadgetKeyFrame);
      this.tabGadgets.Controls.Add(this.lblGadgetKeyFrame);
      this.tabGadgets.Controls.Add(this.picBoxGadget);
      this.tabGadgets.Controls.Add(this.checkGadgetResizeVert);
      this.tabGadgets.Controls.Add(this.checkGadgetResizeHoriz);
      this.tabGadgets.Controls.Add(this.lblGadgetResize);
      this.tabGadgets.Controls.Add(this.numGadgetTriggerH);
      this.tabGadgets.Controls.Add(this.lblGadgetTriggerH);
      this.tabGadgets.Controls.Add(this.numGadgetTriggerW);
      this.tabGadgets.Controls.Add(this.lblGadgetTriggerW);
      this.tabGadgets.Controls.Add(this.numGadgetTriggerY);
      this.tabGadgets.Controls.Add(this.lblGadgetTriggerY);
      this.tabGadgets.Controls.Add(this.numGadgetTriggerX);
      this.tabGadgets.Controls.Add(this.lblGadgetTriggerX);
      this.tabGadgets.Controls.Add(this.lblGadgetTrigger);
      this.tabGadgets.Controls.Add(this.numGadgetFrames);
      this.tabGadgets.Controls.Add(this.lblGadgetFrames);
      this.tabGadgets.Controls.Add(this.cmbGadgetType);
      this.tabGadgets.Controls.Add(this.lblGadgetType);
      this.tabGadgets.Controls.Add(this.txtGadgetName);
      this.tabGadgets.Controls.Add(this.lblGadgetName);
      this.tabGadgets.Controls.Add(this.butGadgetRemove);
      this.tabGadgets.Controls.Add(this.butGadgetAdd);
      this.tabGadgets.Controls.Add(this.listBoxGadgets);
      this.tabGadgets.Location = new System.Drawing.Point(4, 22);
      this.tabGadgets.Name = "tabGadgets";
      this.tabGadgets.Padding = new System.Windows.Forms.Padding(3);
      this.tabGadgets.Size = new System.Drawing.Size(408, 388);
      this.tabGadgets.TabIndex = 2;
      this.tabGadgets.Text = "Objects";
      this.tabGadgets.UseVisualStyleBackColor = true;
      this.tabGadgets.Enter += new System.EventHandler(this.tabGadgets_Enter);
      this.tabGadgets.Leave += new System.EventHandler(this.tabGadgets_Leave);
      // 
      // txtGadgetSound
      // 
      this.txtGadgetSound.Location = new System.Drawing.Point(202, 213);
      this.txtGadgetSound.Name = "txtGadgetSound";
      this.txtGadgetSound.Size = new System.Drawing.Size(198, 20);
      this.txtGadgetSound.TabIndex = 28;
      this.txtGadgetSound.Leave += new System.EventHandler(this.txtGadgetSound_Leave);
      // 
      // lblGadgetSound
      // 
      this.lblGadgetSound.AutoSize = true;
      this.lblGadgetSound.Location = new System.Drawing.Point(141, 216);
      this.lblGadgetSound.Name = "lblGadgetSound";
      this.lblGadgetSound.Size = new System.Drawing.Size(38, 13);
      this.lblGadgetSound.TabIndex = 27;
      this.lblGadgetSound.Text = "Sound";
      // 
      // numGadgetKeyFrame
      // 
      this.numGadgetKeyFrame.Location = new System.Drawing.Point(202, 187);
      this.numGadgetKeyFrame.Name = "numGadgetKeyFrame";
      this.numGadgetKeyFrame.Size = new System.Drawing.Size(57, 20);
      this.numGadgetKeyFrame.TabIndex = 26;
      this.numGadgetKeyFrame.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGadgetKeyFrame.ValueChanged += new System.EventHandler(this.numGadgetKeyFrame_ValueChanged);
      // 
      // lblGadgetKeyFrame
      // 
      this.lblGadgetKeyFrame.AutoSize = true;
      this.lblGadgetKeyFrame.Location = new System.Drawing.Point(141, 189);
      this.lblGadgetKeyFrame.Name = "lblGadgetKeyFrame";
      this.lblGadgetKeyFrame.Size = new System.Drawing.Size(57, 13);
      this.lblGadgetKeyFrame.TabIndex = 25;
      this.lblGadgetKeyFrame.Text = "Key-Frame";
      // 
      // picBoxGadget
      // 
      this.picBoxGadget.BackColor = System.Drawing.Color.Black;
      this.picBoxGadget.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.picBoxGadget.Location = new System.Drawing.Point(144, 239);
      this.picBoxGadget.Name = "picBoxGadget";
      this.picBoxGadget.Size = new System.Drawing.Size(256, 145);
      this.picBoxGadget.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.picBoxGadget.TabIndex = 24;
      this.picBoxGadget.TabStop = false;
      // 
      // checkGadgetResizeVert
      // 
      this.checkGadgetResizeVert.AutoSize = true;
      this.checkGadgetResizeVert.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.checkGadgetResizeVert.Location = new System.Drawing.Point(273, 164);
      this.checkGadgetResizeVert.Name = "checkGadgetResizeVert";
      this.checkGadgetResizeVert.Size = new System.Drawing.Size(48, 17);
      this.checkGadgetResizeVert.TabIndex = 23;
      this.checkGadgetResizeVert.Text = "Vert.";
      this.checkGadgetResizeVert.UseVisualStyleBackColor = true;
      this.checkGadgetResizeVert.CheckedChanged += new System.EventHandler(this.checkGadgetResizeVert_CheckedChanged);
      // 
      // checkGadgetResizeHoriz
      // 
      this.checkGadgetResizeHoriz.AutoSize = true;
      this.checkGadgetResizeHoriz.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.checkGadgetResizeHoriz.Location = new System.Drawing.Point(202, 164);
      this.checkGadgetResizeHoriz.Name = "checkGadgetResizeHoriz";
      this.checkGadgetResizeHoriz.Size = new System.Drawing.Size(53, 17);
      this.checkGadgetResizeHoriz.TabIndex = 22;
      this.checkGadgetResizeHoriz.Text = "Horiz.";
      this.checkGadgetResizeHoriz.UseVisualStyleBackColor = true;
      this.checkGadgetResizeHoriz.CheckedChanged += new System.EventHandler(this.checkGadgetResizeHoriz_CheckedChanged);
      // 
      // lblGadgetResize
      // 
      this.lblGadgetResize.AutoSize = true;
      this.lblGadgetResize.Location = new System.Drawing.Point(141, 165);
      this.lblGadgetResize.Name = "lblGadgetResize";
      this.lblGadgetResize.Size = new System.Drawing.Size(53, 13);
      this.lblGadgetResize.TabIndex = 21;
      this.lblGadgetResize.Text = "Resizable";
      // 
      // numGadgetTriggerH
      // 
      this.numGadgetTriggerH.Location = new System.Drawing.Point(334, 138);
      this.numGadgetTriggerH.Name = "numGadgetTriggerH";
      this.numGadgetTriggerH.Size = new System.Drawing.Size(56, 20);
      this.numGadgetTriggerH.TabIndex = 20;
      this.numGadgetTriggerH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGadgetTriggerH.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numGadgetTriggerH.ValueChanged += new System.EventHandler(this.numGadgetTrigger_ValueChanged);
      this.numGadgetTriggerH.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numGadgetTrigger_KeyUp);
      // 
      // lblGadgetTriggerH
      // 
      this.lblGadgetTriggerH.AutoSize = true;
      this.lblGadgetTriggerH.Location = new System.Drawing.Point(306, 138);
      this.lblGadgetTriggerH.Name = "lblGadgetTriggerH";
      this.lblGadgetTriggerH.Size = new System.Drawing.Size(15, 13);
      this.lblGadgetTriggerH.TabIndex = 19;
      this.lblGadgetTriggerH.Text = "H";
      // 
      // numGadgetTriggerW
      // 
      this.numGadgetTriggerW.Location = new System.Drawing.Point(219, 138);
      this.numGadgetTriggerW.Name = "numGadgetTriggerW";
      this.numGadgetTriggerW.Size = new System.Drawing.Size(56, 20);
      this.numGadgetTriggerW.TabIndex = 18;
      this.numGadgetTriggerW.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGadgetTriggerW.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numGadgetTriggerW.ValueChanged += new System.EventHandler(this.numGadgetTrigger_ValueChanged);
      this.numGadgetTriggerW.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numGadgetTrigger_KeyUp);
      // 
      // lblGadgetTriggerW
      // 
      this.lblGadgetTriggerW.AutoSize = true;
      this.lblGadgetTriggerW.Location = new System.Drawing.Point(199, 140);
      this.lblGadgetTriggerW.Name = "lblGadgetTriggerW";
      this.lblGadgetTriggerW.Size = new System.Drawing.Size(18, 13);
      this.lblGadgetTriggerW.TabIndex = 17;
      this.lblGadgetTriggerW.Text = "W";
      // 
      // numGadgetTriggerY
      // 
      this.numGadgetTriggerY.Location = new System.Drawing.Point(334, 112);
      this.numGadgetTriggerY.Name = "numGadgetTriggerY";
      this.numGadgetTriggerY.Size = new System.Drawing.Size(56, 20);
      this.numGadgetTriggerY.TabIndex = 16;
      this.numGadgetTriggerY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGadgetTriggerY.ValueChanged += new System.EventHandler(this.numGadgetTrigger_ValueChanged);
      this.numGadgetTriggerY.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numGadgetTrigger_KeyUp);
      // 
      // lblGadgetTriggerY
      // 
      this.lblGadgetTriggerY.AutoSize = true;
      this.lblGadgetTriggerY.Location = new System.Drawing.Point(307, 114);
      this.lblGadgetTriggerY.Name = "lblGadgetTriggerY";
      this.lblGadgetTriggerY.Size = new System.Drawing.Size(14, 13);
      this.lblGadgetTriggerY.TabIndex = 15;
      this.lblGadgetTriggerY.Text = "Y";
      // 
      // numGadgetTriggerX
      // 
      this.numGadgetTriggerX.Location = new System.Drawing.Point(219, 112);
      this.numGadgetTriggerX.Name = "numGadgetTriggerX";
      this.numGadgetTriggerX.Size = new System.Drawing.Size(56, 20);
      this.numGadgetTriggerX.TabIndex = 14;
      this.numGadgetTriggerX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGadgetTriggerX.ValueChanged += new System.EventHandler(this.numGadgetTrigger_ValueChanged);
      this.numGadgetTriggerX.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numGadgetTrigger_KeyUp);
      // 
      // lblGadgetTriggerX
      // 
      this.lblGadgetTriggerX.AutoSize = true;
      this.lblGadgetTriggerX.Location = new System.Drawing.Point(199, 114);
      this.lblGadgetTriggerX.Name = "lblGadgetTriggerX";
      this.lblGadgetTriggerX.Size = new System.Drawing.Size(14, 13);
      this.lblGadgetTriggerX.TabIndex = 13;
      this.lblGadgetTriggerX.Text = "X";
      // 
      // lblGadgetTrigger
      // 
      this.lblGadgetTrigger.AutoSize = true;
      this.lblGadgetTrigger.Location = new System.Drawing.Point(141, 114);
      this.lblGadgetTrigger.Name = "lblGadgetTrigger";
      this.lblGadgetTrigger.Size = new System.Drawing.Size(40, 13);
      this.lblGadgetTrigger.TabIndex = 12;
      this.lblGadgetTrigger.Text = "Trigger";
      // 
      // numGadgetFrames
      // 
      this.numGadgetFrames.Location = new System.Drawing.Point(202, 90);
      this.numGadgetFrames.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numGadgetFrames.Name = "numGadgetFrames";
      this.numGadgetFrames.Size = new System.Drawing.Size(57, 20);
      this.numGadgetFrames.TabIndex = 11;
      this.numGadgetFrames.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numGadgetFrames.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numGadgetFrames.ValueChanged += new System.EventHandler(this.numGadgetFrames_ValueChanged);
      // 
      // lblGadgetFrames
      // 
      this.lblGadgetFrames.AutoSize = true;
      this.lblGadgetFrames.Location = new System.Drawing.Point(141, 92);
      this.lblGadgetFrames.Name = "lblGadgetFrames";
      this.lblGadgetFrames.Size = new System.Drawing.Size(41, 13);
      this.lblGadgetFrames.TabIndex = 10;
      this.lblGadgetFrames.Text = "Frames";
      // 
      // cmbGadgetType
      // 
      this.cmbGadgetType.FormattingEnabled = true;
      this.cmbGadgetType.Items.AddRange(new object[] {
            "Movable Background",
            "Hatch",
            "Exit",
            "Trap",
            "Single-Use Trap",
            "Water",
            "Fire",
            "One-way arrows Right",
            "One-way arrows Left",
            "One-way arrows Down",
            "Pick-up Skill",
            "Teleporter",
            "Receiver",
            "Locked Exit",
            "Button",
            "Updraft",
            "Splat-pad",
            "Right-Turning Field",
            "Left-Turning Field",
            "Splitter",
            "None"});
      this.cmbGadgetType.Location = new System.Drawing.Point(202, 67);
      this.cmbGadgetType.Name = "cmbGadgetType";
      this.cmbGadgetType.Size = new System.Drawing.Size(198, 21);
      this.cmbGadgetType.TabIndex = 9;
      this.cmbGadgetType.SelectedIndexChanged += new System.EventHandler(this.cmbGadgetType_SelectedIndexChanged);
      // 
      // lblGadgetType
      // 
      this.lblGadgetType.AutoSize = true;
      this.lblGadgetType.Location = new System.Drawing.Point(141, 70);
      this.lblGadgetType.Name = "lblGadgetType";
      this.lblGadgetType.Size = new System.Drawing.Size(31, 13);
      this.lblGadgetType.TabIndex = 8;
      this.lblGadgetType.Text = "Type";
      // 
      // txtGadgetName
      // 
      this.txtGadgetName.Location = new System.Drawing.Point(202, 44);
      this.txtGadgetName.Name = "txtGadgetName";
      this.txtGadgetName.Size = new System.Drawing.Size(198, 20);
      this.txtGadgetName.TabIndex = 7;
      this.txtGadgetName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGadgetName_KeyDown);
      this.txtGadgetName.Leave += new System.EventHandler(this.txtGadgetName_Leave);
      // 
      // lblGadgetName
      // 
      this.lblGadgetName.AutoSize = true;
      this.lblGadgetName.Location = new System.Drawing.Point(141, 47);
      this.lblGadgetName.Name = "lblGadgetName";
      this.lblGadgetName.Size = new System.Drawing.Size(35, 13);
      this.lblGadgetName.TabIndex = 6;
      this.lblGadgetName.Text = "Name";
      // 
      // butGadgetRemove
      // 
      this.butGadgetRemove.Location = new System.Drawing.Point(219, 5);
      this.butGadgetRemove.Name = "butGadgetRemove";
      this.butGadgetRemove.Size = new System.Drawing.Size(71, 30);
      this.butGadgetRemove.TabIndex = 5;
      this.butGadgetRemove.Text = "Remove";
      this.butGadgetRemove.UseVisualStyleBackColor = true;
      this.butGadgetRemove.Click += new System.EventHandler(this.butGadgetRemove_Click);
      // 
      // butGadgetAdd
      // 
      this.butGadgetAdd.Location = new System.Drawing.Point(142, 5);
      this.butGadgetAdd.Name = "butGadgetAdd";
      this.butGadgetAdd.Size = new System.Drawing.Size(71, 30);
      this.butGadgetAdd.TabIndex = 4;
      this.butGadgetAdd.Text = "Add";
      this.butGadgetAdd.UseVisualStyleBackColor = true;
      this.butGadgetAdd.Click += new System.EventHandler(this.butGadgetAdd_Click);
      // 
      // listBoxGadgets
      // 
      this.listBoxGadgets.FormattingEnabled = true;
      this.listBoxGadgets.Location = new System.Drawing.Point(3, 3);
      this.listBoxGadgets.Name = "listBoxGadgets";
      this.listBoxGadgets.Size = new System.Drawing.Size(130, 381);
      this.listBoxGadgets.Sorted = true;
      this.listBoxGadgets.TabIndex = 3;
      this.listBoxGadgets.SelectedIndexChanged += new System.EventHandler(this.listBoxGadgets_SelectedIndexChanged);
      // 
      // tabBackground
      // 
      this.tabBackground.Controls.Add(this.txtBackgroundName);
      this.tabBackground.Controls.Add(this.lblBackgroundName);
      this.tabBackground.Controls.Add(this.picBackground);
      this.tabBackground.Controls.Add(this.butBackgroundRemove);
      this.tabBackground.Controls.Add(this.butBackgroundAdd);
      this.tabBackground.Controls.Add(this.listBoxBackgrounds);
      this.tabBackground.Location = new System.Drawing.Point(4, 22);
      this.tabBackground.Name = "tabBackground";
      this.tabBackground.Padding = new System.Windows.Forms.Padding(3);
      this.tabBackground.Size = new System.Drawing.Size(408, 388);
      this.tabBackground.TabIndex = 3;
      this.tabBackground.Text = "Background";
      this.tabBackground.UseVisualStyleBackColor = true;
      this.tabBackground.Enter += new System.EventHandler(this.tabBackground_Enter);
      // 
      // txtBackgroundName
      // 
      this.txtBackgroundName.Location = new System.Drawing.Point(202, 44);
      this.txtBackgroundName.Name = "txtBackgroundName";
      this.txtBackgroundName.Size = new System.Drawing.Size(198, 20);
      this.txtBackgroundName.TabIndex = 30;
      this.txtBackgroundName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBackgroundName_KeyDown);
      this.txtBackgroundName.Leave += new System.EventHandler(this.txtBackgroundName_Leave);
      // 
      // lblBackgroundName
      // 
      this.lblBackgroundName.AutoSize = true;
      this.lblBackgroundName.Location = new System.Drawing.Point(141, 47);
      this.lblBackgroundName.Name = "lblBackgroundName";
      this.lblBackgroundName.Size = new System.Drawing.Size(35, 13);
      this.lblBackgroundName.TabIndex = 29;
      this.lblBackgroundName.Text = "Name";
      // 
      // picBackground
      // 
      this.picBackground.BackColor = System.Drawing.Color.Black;
      this.picBackground.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.picBackground.Location = new System.Drawing.Point(144, 70);
      this.picBackground.Name = "picBackground";
      this.picBackground.Size = new System.Drawing.Size(256, 316);
      this.picBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.picBackground.TabIndex = 28;
      this.picBackground.TabStop = false;
      // 
      // butBackgroundRemove
      // 
      this.butBackgroundRemove.Location = new System.Drawing.Point(219, 5);
      this.butBackgroundRemove.Name = "butBackgroundRemove";
      this.butBackgroundRemove.Size = new System.Drawing.Size(71, 30);
      this.butBackgroundRemove.TabIndex = 27;
      this.butBackgroundRemove.Text = "Remove";
      this.butBackgroundRemove.UseVisualStyleBackColor = true;
      this.butBackgroundRemove.Click += new System.EventHandler(this.butBackgroundRemove_Click);
      // 
      // butBackgroundAdd
      // 
      this.butBackgroundAdd.Location = new System.Drawing.Point(142, 5);
      this.butBackgroundAdd.Name = "butBackgroundAdd";
      this.butBackgroundAdd.Size = new System.Drawing.Size(71, 30);
      this.butBackgroundAdd.TabIndex = 26;
      this.butBackgroundAdd.Text = "Add";
      this.butBackgroundAdd.UseVisualStyleBackColor = true;
      this.butBackgroundAdd.Click += new System.EventHandler(this.butBackgroundAdd_Click);
      // 
      // listBoxBackgrounds
      // 
      this.listBoxBackgrounds.FormattingEnabled = true;
      this.listBoxBackgrounds.Location = new System.Drawing.Point(5, 5);
      this.listBoxBackgrounds.Name = "listBoxBackgrounds";
      this.listBoxBackgrounds.Size = new System.Drawing.Size(130, 381);
      this.listBoxBackgrounds.Sorted = true;
      this.listBoxBackgrounds.TabIndex = 25;
      this.listBoxBackgrounds.SelectedIndexChanged += new System.EventHandler(this.listBoxBackgrounds_SelectedIndexChanged);
      // 
      // timerPicGadget
      // 
      this.timerPicGadget.Interval = 60;
      this.timerPicGadget.Tick += new System.EventHandler(this.timerPicGadget_Tick);
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(416, 441);
      this.Controls.Add(this.tabMain);
      this.Controls.Add(this.menuStrip1);
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "FormMain";
      this.Text = "NeoLemmix Graphics Tool";
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.tabMain.ResumeLayout(false);
      this.tabGeneral.ResumeLayout(false);
      this.tabGeneral.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picColor)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numBlue)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGreen)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numRed)).EndInit();
      this.tabTerrain.ResumeLayout(false);
      this.tabTerrain.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picTerrain)).EndInit();
      this.tabGadgets.ResumeLayout(false);
      this.tabGadgets.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetKeyFrame)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picBoxGadget)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerH)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerW)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetTriggerX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numGadgetFrames)).EndInit();
      this.tabBackground.ResumeLayout(false);
      this.tabBackground.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.picBackground)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.PictureBox picColor;
        private System.Windows.Forms.NumericUpDown numBlue;
        private System.Windows.Forms.Label lblBlue;
        private System.Windows.Forms.NumericUpDown numGreen;
        private System.Windows.Forms.Label lblGreen;
        private System.Windows.Forms.NumericUpDown numRed;
        private System.Windows.Forms.Label lblRed;
        private System.Windows.Forms.Label lblStyleColors;
        private System.Windows.Forms.ComboBox cmbColorTypes;
        private System.Windows.Forms.TextBox txtStyleName;
        private System.Windows.Forms.Label lblStyleName;
        private System.Windows.Forms.TabPage tabTerrain;
        private System.Windows.Forms.Button butTerrainRemove;
        private System.Windows.Forms.Button butTerrainAdd;
        private System.Windows.Forms.ListBox listBoxTerrain;
        private System.Windows.Forms.TabPage tabGadgets;
        private System.Windows.Forms.TabPage tabBackground;
        private System.Windows.Forms.ComboBox cmbGadgetType;
        private System.Windows.Forms.Label lblGadgetType;
        private System.Windows.Forms.TextBox txtGadgetName;
        private System.Windows.Forms.Label lblGadgetName;
        private System.Windows.Forms.Button butGadgetRemove;
        private System.Windows.Forms.Button butGadgetAdd;
        private System.Windows.Forms.ListBox listBoxGadgets;
        private System.Windows.Forms.PictureBox picTerrain;
        private System.Windows.Forms.CheckBox checkTerrainSteel;
        private System.Windows.Forms.TextBox txtTerrainName;
        private System.Windows.Forms.Label lblTerrainName;
        private System.Windows.Forms.PictureBox picBoxGadget;
        private System.Windows.Forms.CheckBox checkGadgetResizeVert;
        private System.Windows.Forms.CheckBox checkGadgetResizeHoriz;
        private System.Windows.Forms.Label lblGadgetResize;
        private System.Windows.Forms.NumericUpDown numGadgetTriggerH;
        private System.Windows.Forms.Label lblGadgetTriggerH;
        private System.Windows.Forms.NumericUpDown numGadgetTriggerW;
        private System.Windows.Forms.Label lblGadgetTriggerW;
        private System.Windows.Forms.NumericUpDown numGadgetTriggerY;
        private System.Windows.Forms.Label lblGadgetTriggerY;
        private System.Windows.Forms.NumericUpDown numGadgetTriggerX;
        private System.Windows.Forms.Label lblGadgetTriggerX;
        private System.Windows.Forms.Label lblGadgetTrigger;
        private System.Windows.Forms.NumericUpDown numGadgetFrames;
        private System.Windows.Forms.Label lblGadgetFrames;
        private System.Windows.Forms.Timer timerPicGadget;
        private System.Windows.Forms.TextBox txtBackgroundName;
        private System.Windows.Forms.Label lblBackgroundName;
        private System.Windows.Forms.PictureBox picBackground;
        private System.Windows.Forms.Button butBackgroundRemove;
        private System.Windows.Forms.Button butBackgroundAdd;
        private System.Windows.Forms.ListBox listBoxBackgrounds;
        private System.Windows.Forms.TextBox txtGadgetSound;
        private System.Windows.Forms.Label lblGadgetSound;
        private System.Windows.Forms.NumericUpDown numGadgetKeyFrame;
        private System.Windows.Forms.Label lblGadgetKeyFrame;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

