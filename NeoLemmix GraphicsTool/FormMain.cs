﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NeoLemmix_GraphicsTool
{
  partial class FormMain : Form
  {
    public FormMain()
    {
      InitializeComponent();
      curStyle = new Style();
      cmbColorTypes.SelectedIndex = 0;
      curTimerFrame = 0;
      isWriteToForm = false;

      listBoxTerrain.Items.Clear();
      listBoxGadgets.Items.Clear();
      listBoxBackgrounds.Items.Clear();
    }

    public Style curStyle;
    private int curTimerFrame;
    private bool isWriteToForm;

    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void loadToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Style newStyle = Read.Style();
      if (newStyle == null) return;

      curStyle = newStyle;

      // Go back to first tab
      tabMain.SelectedIndex = 0;
      txtStyleName.Text = curStyle.Name;
      cmbColorTypes.SelectedIndex = 0;
      cmbColorTypes_SelectedIndexChanged(null, null);

      // Update all list items
      listBoxTerrain.Items.Clear();
      foreach (Terrain piece in curStyle.Terrains)
      {
        listBoxTerrain.Items.Add(piece.Name);
      }
      listBoxGadgets.Items.Clear();
      foreach (Gadget piece in curStyle.Gadgets)
      {
        listBoxGadgets.Items.Add(piece.Name);
      }
      listBoxBackgrounds.Items.Clear();
      foreach (Background piece in curStyle.Backgrounds)
      {
        listBoxBackgrounds.Items.Add(piece.Name);
      }

    }

    private void saveToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        if (Save.Style(curStyle))
        {
          MessageBox.Show("Style successfully saved!", "Style saved");
        }
      }
      catch (Exception Ex)
      {
        MessageBox.Show("Could not save the style: " + Ex.Message, "Error saving the style");
      }
    }

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Form versionForm = new Form();
      versionForm.Width = 310;
      versionForm.Height = 170;
      versionForm.MaximizeBox = false;
      versionForm.ShowInTaskbar = false;
      versionForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      versionForm.Text = "NeoLemmix Graphics Tool - About";

      versionForm.Show();

      using (Graphics aboutGraphics = versionForm.CreateGraphics())
      using (SolidBrush brush = new SolidBrush(Color.Black))
      using (Font font = new Font("Microsoft Sans Serif", 8))
      {
        for (int i = 0; i < C.VersionList.Count; i++)
        {
          aboutGraphics.DrawString(C.VersionList[i], font, brush, 6, 6 + 13 * i);
        }
      }
    }

    /*----------------------------------------
                GENERAL TAB
      ----------------------------------------*/

    /// <summary>
    /// Reads the new style name
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtStyleName_TextChanged(object sender, EventArgs e)
    {
      curStyle.Name = txtStyleName.Text;
    }

    /// <summary>
    /// Determines the selected color type and changes the RGB values accordingly.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cmbColorTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
      C.ThemeColor colorType = (C.ThemeColor)cmbColorTypes.SelectedIndex;

      isWriteToForm = true;
      numRed.Value = curStyle.ThemeColors[colorType].R;
      numGreen.Value = curStyle.ThemeColors[colorType].G;
      numBlue.Value = curStyle.ThemeColors[colorType].B;
      isWriteToForm = false;

      // Update picture color 
      numColor_ValueChanged(null, null);
    }

    /// <summary>
    /// Reads the chosen color values in and displays the color in the picturebox next to it.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void numColor_ValueChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      C.ThemeColor colorType = (C.ThemeColor)cmbColorTypes.SelectedIndex;
      curStyle.ThemeColors[colorType] = Color.FromArgb(255, (int)numRed.Value, (int)numGreen.Value, (int)numBlue.Value);
      picColor.BackColor = curStyle.ThemeColors[colorType];
    }

    /*----------------------------------------
                TERRAIN TAB
      ----------------------------------------*/

    private void tabTerrain_Enter(object sender, EventArgs e)
    {
      // Select first item, if possible
      if (curStyle.Terrains.Count > 0) listBoxTerrain.SelectedIndex = 0;
      SetTerrainButtons();
    }

    /// <summary>
    /// Disables the remove terrain button, if appropriate.
    /// </summary>
    private void SetTerrainButtons()
    {
      butTerrainRemove.Enabled = (GetSelectedTerrain() != null);
    }

    /// <summary>
    /// Gets the currently selected terrain piece, or null if none is selected.
    /// </summary>
    /// <returns></returns>
    private Terrain GetSelectedTerrain()
    {
      curStyle.Terrains.Sort();
      if (listBoxTerrain.SelectedIndex < 0 || listBoxTerrain.SelectedIndex >= curStyle.Terrains.Count)
      {
        return null;
      }
      else
      {
        return curStyle.Terrains[listBoxTerrain.SelectedIndex];
      }
    }

    /// <summary>
    /// Update displayed terrain piece.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listBoxTerrain_SelectedIndexChanged(object sender, EventArgs e)
    {
      Terrain piece = GetSelectedTerrain();
      if (piece == null) return;

      isWriteToForm = true;
      txtTerrainName.Text = piece.Name;
      checkTerrainSteel.Checked = piece.IsSteel;
      picTerrain.Image = piece.Image.Zoom(2);
      isWriteToForm = false;

      SetTerrainButtons();
    }

    /// <summary>
    /// Adds the terrain pieces selected by the user via a file-browser.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butTerrainAdd_Click(object sender, EventArgs e)
    {
      string filter = "Image files (*.png)|*.png";
      string[] filePaths = Utility.SelectFiles(filter, true);

      foreach (string filePath in filePaths)
      {
        Terrain piece = new Terrain(filePath);
        curStyle.Terrains.Add(piece);
        listBoxTerrain.Items.Add(piece.Name);
      }

      SetTerrainButtons();
    }

    /// <summary>
    /// Removes the selected terrain piece.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butTerrainRemove_Click(object sender, EventArgs e)
    {
      Terrain piece = GetSelectedTerrain();
      if (piece == null) return;

      curStyle.Terrains.Remove(piece);
      listBoxTerrain.Items.RemoveAt(listBoxTerrain.SelectedIndex);

      SetTerrainButtons();
    }

    /// <summary>
    /// Updates the name of the selected piece.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtTerrainName_Leave(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Terrain piece = GetSelectedTerrain();
      if (piece == null) return;

      if (curStyle.Terrains.Exists(ter => ter.Name == txtTerrainName.Text))
      {
        MessageBox.Show("You may not have two terrain pieces with the same name!");
      }
      else
      {
        piece.Name = txtTerrainName.Text;
        curStyle.Terrains.Sort();

        listBoxTerrain.Items.Clear();
        foreach (Terrain someTerrain in curStyle.Terrains)
        {
          listBoxTerrain.Items.Add(someTerrain.Name);
        }
        listBoxTerrain.SelectedIndex = curStyle.Terrains.IndexOf(piece);
      }
    }

    /// <summary>
    /// Updates the name of the selected piece after hitting Enter.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtTerrainName_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        this.tabTerrain.Focus();
      }
    }

    /// <summary>
    /// Updates whether the selected piece is steel or not.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void checkTerrainSteel_CheckStateChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Terrain piece = GetSelectedTerrain();
      if (piece == null) return;

      piece.IsSteel = checkTerrainSteel.Checked;
    }

    /*----------------------------------------
             GADGET TAB
      ----------------------------------------*/

    private void tabGadgets_Enter(object sender, EventArgs e)
    {
      timerPicGadget.Enabled = true;
      // Select first item, if possible
      if (curStyle.Gadgets.Count > 0) listBoxGadgets.SelectedIndex = 0;
      SelectEnabledGadgetComponents();
    }

    private void tabGadgets_Leave(object sender, EventArgs e)
    {
      timerPicGadget.Enabled = false;
    }

    /// <summary>
    /// Gets the currently selected terrain piece, or null if none is selected.
    /// </summary>
    /// <returns></returns>
    private Gadget GetSelectedGadget()
    {
      curStyle.Gadgets.Sort();
      if (listBoxGadgets.SelectedIndex < 0 || listBoxGadgets.SelectedIndex >= curStyle.Gadgets.Count)
      {
        return null;
      }
      else
      {
        return curStyle.Gadgets[listBoxGadgets.SelectedIndex];
      }
    }

    /// <summary>
    /// Update displayed gadget piece.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listBoxGadgets_SelectedIndexChanged(object sender, EventArgs e)
    {
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      isWriteToForm = true;
      txtGadgetName.Text = piece.Name;
      cmbGadgetType.SelectedIndex = (int)piece.GadgetType;
      numGadgetFrames.Value = piece.NumFrames;
      numGadgetTriggerX.Value = piece.TriggerRect.X;
      numGadgetTriggerY.Value = piece.TriggerRect.Y;
      numGadgetTriggerW.Value = piece.TriggerRect.Width;
      numGadgetTriggerH.Value = piece.TriggerRect.Height;
      checkGadgetResizeHoriz.Checked = piece.IsResizableHoriz;
      checkGadgetResizeVert.Checked = piece.IsResizableVert;
      numGadgetKeyFrame.Value = piece.KeyFrame;
      txtGadgetSound.Text = piece.Sound;
      isWriteToForm = false;

      SelectEnabledGadgetComponents();
    }

    /// <summary>
    /// Enables all settings needed for the selected gadget.
    /// </summary>
    private void SelectEnabledGadgetComponents()
    {
      Gadget piece = GetSelectedGadget();
      butGadgetRemove.Enabled = (piece != null);
      if (piece == null)
      {
        txtGadgetName.Enabled = false;
        cmbGadgetType.Enabled = false;
        numGadgetFrames.Enabled = false;
        numGadgetTriggerX.Enabled = false;
        numGadgetTriggerY.Enabled = false;
        numGadgetTriggerW.Enabled = false;
        numGadgetTriggerH.Enabled = false;
        checkGadgetResizeHoriz.Enabled = false;
        checkGadgetResizeVert.Enabled = false;
        numGadgetKeyFrame.Enabled = false;
        txtGadgetSound.Enabled = false;
      }
      else
      {
        txtGadgetName.Enabled = true;
        cmbGadgetType.Enabled = true;
        numGadgetFrames.Enabled = true; // ???
        numGadgetTriggerX.Enabled = C.HasTrigger(piece);
        numGadgetTriggerY.Enabled = C.HasTrigger(piece);
        numGadgetTriggerW.Enabled = C.HasTriggerArea(piece);
        numGadgetTriggerH.Enabled = C.HasTriggerArea(piece);
        checkGadgetResizeHoriz.Enabled = C.IsResizable(piece);
        checkGadgetResizeVert.Enabled = C.IsResizable(piece);
        numGadgetKeyFrame.Enabled = C.HasKeyFrame(piece);
        txtGadgetSound.Enabled = C.HasSound(piece);
      }
    }

    /// <summary>
    /// Adds all the gadgets selected by the user via a file browser.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butGadgetAdd_Click(object sender, EventArgs e)
    {
      string filter = "Image files (*.png)|*.png";
      string[] filePaths = Utility.SelectFiles(filter, true);

      foreach (string filePath in filePaths)
      {
        Gadget piece = new Gadget(filePath);
        curStyle.Gadgets.Add(piece);
        listBoxGadgets.Items.Add(piece.Name);
      }

      SelectEnabledGadgetComponents();
    }

    /// <summary>
    /// Removes the selected gadget.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butGadgetRemove_Click(object sender, EventArgs e)
    {
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      curStyle.Gadgets.Remove(piece);
      listBoxGadgets.Items.RemoveAt(listBoxGadgets.SelectedIndex);

      SelectEnabledGadgetComponents();
    }

    /// <summary>
    /// Updates the gadget name.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtGadgetName_Leave(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      if (curStyle.Gadgets.Exists(gad => gad.Name == txtGadgetName.Text))
      {
        MessageBox.Show("You may not have two objects with the same name!");
      }
      else
      {
        piece.Name = txtGadgetName.Text;
        curStyle.Gadgets.Sort();

        listBoxGadgets.Items.Clear();
        foreach (Gadget somePiece in curStyle.Gadgets)
        {
          listBoxGadgets.Items.Add(somePiece.Name);
        }
        listBoxGadgets.SelectedIndex = curStyle.Gadgets.IndexOf(piece);
      }
    }

    /// <summary>
    /// Updates the name of the selected gadget after hitting Enter.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtGadgetName_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        this.tabGadgets.Focus();
      }
    }

    /// <summary>
    /// Updates the gadget type.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cmbGadgetType_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      piece.GadgetType = (C.GadgetType)cmbGadgetType.SelectedIndex;

      SelectEnabledGadgetComponents();
    }

    /// <summary>
    /// Updates the number of frames in the sprite.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void numGadgetFrames_ValueChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      int newFrameNumber = (int)numGadgetFrames.Value;
      if (newFrameNumber != piece.NumFrames)
      {
        piece.NumFrames = newFrameNumber;
        piece.SplitImageInFrames();
      }
    }

    /// <summary>
    /// Updates the trigger area of the gadget.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void numGadgetTrigger_ValueChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      piece.TriggerRect = new Rectangle((int)numGadgetTriggerX.Value, (int)numGadgetTriggerY.Value,
                                        (int)numGadgetTriggerW.Value, (int)numGadgetTriggerH.Value);
    }

    /// <summary>
    /// Updates the trigger area of the gadget.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void numGadgetTrigger_KeyUp(object sender, KeyEventArgs e)
    {
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      string newText = (sender as NumericUpDown).Text;
      int newValue;
      if (!int.TryParse(newText, out newValue)) return;

      int xPos = sender.Equals(numGadgetTriggerX) ? newValue : (int)numGadgetTriggerX.Value;
      int yPos = sender.Equals(numGadgetTriggerY) ? newValue : (int)numGadgetTriggerY.Value;
      int width = sender.Equals(numGadgetTriggerW) ? newValue : (int)numGadgetTriggerW.Value;
      int height = sender.Equals(numGadgetTriggerH) ? newValue : (int)numGadgetTriggerH.Value;
      piece.TriggerRect = new Rectangle(xPos, yPos, width, height);
    }

    /// <summary>
    /// Updates whether one can resize the gadget horizontally.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void checkGadgetResizeHoriz_CheckedChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      piece.IsResizableHoriz = checkGadgetResizeHoriz.Checked;
    }

    /// <summary>
    /// Updates whether one can resize the gadget vertically.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void checkGadgetResizeVert_CheckedChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      piece.IsResizableVert = checkGadgetResizeVert.Checked;
    }

    /// <summary>
    /// Updates the key-frame value of the gadget.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void numGadgetKeyFrame_ValueChanged(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      piece.KeyFrame = (int)numGadgetKeyFrame.Value;
    }

    /// <summary>
    /// Updates the sound of the gadget.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtGadgetSound_Leave(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      piece.Sound = txtGadgetSound.Text;
    }

    /// <summary>
    /// Updates the rendered picture of the gadget.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void timerPicGadget_Tick(object sender, EventArgs e)
    {
      Gadget piece = GetSelectedGadget();
      if (piece == null) return;

      curTimerFrame++;
      Bitmap curSprite;

      if (C.HasTrigger(piece))
      {
        // Get size of the sprite with trigger area 
        int border = piece.TriggerOversize();
        curSprite = new Bitmap(piece.SpriteOversize().Width, piece.SpriteOversize().Height);

        using (Graphics g = Graphics.FromImage(curSprite))
        {
          // Draw the sprite in the center
          g.DrawImage(piece.BitmapFrames[curTimerFrame % piece.NumFrames], new Point(border, border));

          // Add the trigger area
          Color brushColor = Color.FromArgb(160, 238, 130, 238);
          Rectangle triggerRect = new Rectangle(piece.TriggerRect.X * 2 + border, piece.TriggerRect.Y * 2 + border,
                                                piece.TriggerRect.Width * 2, piece.TriggerRect.Height * 2);
          using (Brush b = new SolidBrush(brushColor))
          {
            g.FillRectangle(b, triggerRect);
          }
        }
      }
      else
      {
        curSprite = (Bitmap)piece.BitmapFrames[curTimerFrame % piece.NumFrames].Clone();
      }

      picBoxGadget.Image = curSprite;

      // Stop animation at the SleepFrame:
      bool isSleepFrame = (curTimerFrame % piece.NumFrames == C.getSleepFrame(piece.GadgetType));
      this.timerPicGadget.Interval = isSleepFrame ? 3000 : 60;
    }

    /*----------------------------------------
               BACKGROUND TAB
      ----------------------------------------*/

    private void tabBackground_Enter(object sender, EventArgs e)
    {
      // Select first item, if possible
      if (curStyle.Backgrounds.Count > 0) listBoxBackgrounds.SelectedIndex = 0;
      SetBackgroundButtons();
    }

    /// <summary>
    /// Disables the remove background button, if appropriate.
    /// </summary>
    private void SetBackgroundButtons()
    {
      butBackgroundRemove.Enabled = (GetSelectedBackground() != null);
    }

    /// <summary>
    /// Gets the currently selected terrain piece, or null if none is selected.
    /// </summary>
    /// <returns></returns>
    private Background GetSelectedBackground()
    {
      curStyle.Backgrounds.Sort();
      if (listBoxBackgrounds.SelectedIndex < 0 || listBoxBackgrounds.SelectedIndex >= curStyle.Backgrounds.Count)
      {
        return null;
      }
      else
      {
        return curStyle.Backgrounds[listBoxBackgrounds.SelectedIndex];
      }
    }

    /// <summary>
    /// Update displayed background piece.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listBoxBackgrounds_SelectedIndexChanged(object sender, EventArgs e)
    {
      Background piece = GetSelectedBackground();
      if (piece == null) return;

      isWriteToForm = true;
      txtBackgroundName.Text = piece.Name;
      picBackground.Image = piece.Image;
      isWriteToForm = false;

      SetBackgroundButtons();
    }

    /// <summary>
    /// Adds all the backgrounds selected by the user via a file browser.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butBackgroundAdd_Click(object sender, EventArgs e)
    {
      string filter = "Image files (*.png)|*.png";
      string[] filePaths = Utility.SelectFiles(filter, true);

      foreach (string filePath in filePaths)
      {
        Background piece = new Background(filePath);
        curStyle.Backgrounds.Add(piece);
        listBoxBackgrounds.Items.Add(piece.Name);
      }

      SetBackgroundButtons();
    }

    /// <summary>
    /// Removes the selected background image.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void butBackgroundRemove_Click(object sender, EventArgs e)
    {
      Background piece = GetSelectedBackground();
      if (piece == null) return;

      curStyle.Backgrounds.Remove(piece);
      listBoxBackgrounds.Items.RemoveAt(listBoxBackgrounds.SelectedIndex);

      SetBackgroundButtons();
    }

    /// <summary>
    /// Updates the background name.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtBackgroundName_Leave(object sender, EventArgs e)
    {
      if (isWriteToForm) return;
      Background piece = GetSelectedBackground();
      if (piece == null) return;

      if (curStyle.Backgrounds.Exists(bg => bg.Name == txtBackgroundName.Text))
      {
        MessageBox.Show("You may not have two backgrounds with the same name!");
      }
      else
      {
        piece.Name = txtBackgroundName.Text;
        curStyle.Backgrounds.Sort();

        listBoxBackgrounds.Items.Clear();
        foreach (Background someBG in curStyle.Backgrounds)
        {
          listBoxBackgrounds.Items.Add(someBG.Name);
        }
        listBoxBackgrounds.SelectedIndex = curStyle.Backgrounds.IndexOf(piece);
      }
    }

    /// <summary>
    /// Updates the name of the selected backgroun after hitting Enter.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txtBackgroundName_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        this.tabBackground.Focus();
      }
    }

  }
}
