﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace NeoLemmix_GraphicsTool
{
    class Terrain : IComparable<Terrain>, IPiece
    {
        public Terrain(string imagePath)
        {
            try
            {
                Name = Path.GetFileNameWithoutExtension(imagePath);
                Image = Utility.CreateBitmapFromFile(imagePath);
                IsSteel = false;
            }
            catch
            {
                Name = "unknown";
                Image = new Bitmap(1, 1);
                IsSteel = false;
            }

            string infoPath = Path.ChangeExtension(imagePath, ".nxmt");
            if (File.Exists(infoPath))
            {
                try
                {
                    LoadInfo(infoPath);
                }
                catch
                {
                    // do nothing more
                }
            }
        }

        public string Name { get; set; }
        public Bitmap Image { get; set; }
        public bool IsSteel;

        public int CompareTo(Terrain terrain2)
        {
            return this.Name.CompareTo(terrain2.Name);
        }

        /// <summary>
        /// Load the terrain info, i.e. whether it is steel.
        /// </summary>
        /// <param name="infoPath"></param>
        private void LoadInfo(string infoPath)
        {
            FileParser parser = null;
            try
            {
                parser = new FileParser(infoPath);
                List<FileLine> fileLines;
                while ((fileLines = parser.GetNextLines()) != null)
                {
                    if (fileLines[0].Key == "STEEL")
                    {
                        IsSteel = true;
                    }
                }
            }
            finally
            {
                parser?.DisposeStreamReader();
            }
        }

    }
}
