﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;

namespace NeoLemmix_GraphicsTool
{
  class C
  {
    public static string Version
    {
      get
      {
        var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        return version.Major.ToString() + "." + version.Minor.ToString();
      }
    }

    public static string AppPath => System.Windows.Forms.Application.StartupPath + DirSep;
    public static string StylePath => Directory.Exists(AppPath + "styles") ? AppPath + "styles" : AppPath;

    public static char DirSep => Path.DirectorySeparatorChar;
    public static string NewLine => Environment.NewLine;

    public enum ThemeColor { Background, Minimap, OneWayWall, PickUp, PickUpBorder, Mask }
    public static Array ThemeColorArray => Enum.GetValues(typeof(C.ThemeColor));

    public static readonly Dictionary<ThemeColor, Color> DefaultThemeColor = new Dictionary<ThemeColor, Color>
    {
      { ThemeColor.Background, Color.Black },
      { ThemeColor.Minimap, ColorTranslator.FromHtml("#D08020")},
      { ThemeColor.Mask, ColorTranslator.FromHtml("#D08020")},
      { ThemeColor.OneWayWall, ColorTranslator.FromHtml("#B0B000")},
      { ThemeColor.PickUp, Color.Black },
      { ThemeColor.PickUpBorder, Color.Black },
    };

    public enum GadgetType
    {
      Background, Hatch, Exit, Trap, TrapOnce, Water, Fire,
      OWW_Right, OWW_Left, OWW_Down,
      PickUp, Teleporter, Receiver, ExitLocked, Button,
      Updraft, Splat, Force_Right, Force_Left, Splitter, None
    }

    public static bool HasTrigger(Gadget piece) => !piece.GadgetType.In(GadgetType.None, GadgetType.Background);
    public static bool HasTriggerArea(Gadget piece) => HasTrigger(piece) && !piece.GadgetType.In(GadgetType.Hatch, GadgetType.Receiver);
    public static bool IsResizable(Gadget piece) => piece.GadgetType.In(GadgetType.Trap, GadgetType.TrapOnce,
        GadgetType.Fire, GadgetType.Water, GadgetType.OWW_Left, GadgetType.OWW_Right, GadgetType.OWW_Down,
        GadgetType.Updraft, GadgetType.Splat, GadgetType.Force_Left, GadgetType.Force_Right, GadgetType.Background);
    public static bool HasKeyFrame(Gadget piece) => piece.GadgetType.In(GadgetType.Receiver, GadgetType.Teleporter);
    public static bool HasSound(Gadget piece) => piece.GadgetType.In(GadgetType.Trap, GadgetType.TrapOnce,
        GadgetType.Receiver, GadgetType.Teleporter, GadgetType.ExitLocked, GadgetType.Button);

    public static int getSleepFrame(GadgetType type)
    {
      switch (type)
      {
        case GadgetType.Hatch:
        case GadgetType.Trap:
        case GadgetType.Teleporter:
        case GadgetType.Receiver:
        case GadgetType.ExitLocked:
        case GadgetType.Splitter:
          return 0;
        case GadgetType.TrapOnce:
        case GadgetType.Button:
          return 1;
        default:
          return -1;
      }
    }

    public static readonly List<string> VersionList = new List<string>
    {
      "Version " + Version,
      "   by Stephan Neupert (Nepster)",
      "",
      "Thanks to...",
      "  DMA for creating the original Lemmings games.",
      "  Namida Verasche for the NeoLemmix player.",
      "  The LemmingsForums at http://www.lemmingsforums.net.",
      "",
      "This application and all its source code is licensed under",
      "   CC BY-NC 4.0."
    };
  }
}
